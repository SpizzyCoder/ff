use std::path::Path;

use clap::Parser;
use walkdir::{DirEntry, IntoIter, WalkDir};
use wildmatch::WildMatch;

/// Simple program to search for files and directories
#[derive(Parser, Debug)]
#[command(about, version)]
struct Args {
    /// Search string
    #[arg()]
    search_string: String,

    /// Root dir
    #[arg(long, short, default_value = ".")]
    root_dir: String,

    /// Ignore case
    #[arg(long, short)]
    ignore_case: bool,

    /// Verbose (print errors)
    #[arg(long, short)]
    verbose: bool,
}

fn main() {
    let args: Args = Args::parse();

    if Path::new(&args.root_dir).is_dir() == false {
        eprintln!["ERROR: {} is not a directory!", args.root_dir];
        return;
    }

    let search_string: String;

    if args.ignore_case {
        search_string = args.search_string.to_lowercase();
    } else {
        search_string = args.search_string.clone();
    }

    let mut counter: u32 = 0;
    let mut iterator: IntoIter = WalkDir::new(&args.root_dir).into_iter();
    iterator.next(); // Ignore first element (root folder itself)

    for entry in iterator {
        let entry_unwrapped: DirEntry;

        match entry {
            Ok(dir_entry) => entry_unwrapped = dir_entry,
            Err(err) => {
                if args.verbose {
                    eprintln!["{}", err];
                }
                continue;
            },
        }

        let mut filename_as_string: String;

        match entry_unwrapped.path().file_name() {
            Some(osstr) => match osstr.to_str() {
                Some(str) => filename_as_string = str.to_owned(),
                None => {
                    if args.verbose {
                        eprintln!["Failed to convert OsStr to str ({:?})", osstr];
                    }
                    continue;
                },
            },
            None => {
                if args.verbose {
                    eprintln!["Failed to get filename from {}", entry_unwrapped.path().display()];
                }
                continue;
            },
        }

        if args.ignore_case {
            filename_as_string = filename_as_string.to_lowercase();
        }

        if WildMatch::new(&search_string).matches(&filename_as_string) {
            counter += 1;
            println!["{}: {}", counter, entry_unwrapped.path().display()];
        }
    }

    if counter == 0 {
        println!["No matches"];
    }
}